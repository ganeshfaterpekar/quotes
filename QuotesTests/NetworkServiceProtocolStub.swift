//
//  NetworkServiceProtocolStub.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation
import Combine

class NetworkServiceProtocolSub : NetworkServiceProtocol {
    let result : Result<Data,Error>
    
    init(result : Result<Data,Error>) {
            self.result = result
    }
    
    func loadRequest<T>(urlRequest: URLRequest) -> AnyPublisher<T, Error> where T : Decodable {
        return result.publisher.tryMap { result in
            let decoder = JSONDecoder()
            return try decoder.decode(T.self, from: result)
        }.eraseToAnyPublisher()
    }

}

