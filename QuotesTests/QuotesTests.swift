//
//  QuotesTests.swift
//  QuotesTests
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import XCTest
import Combine
@testable import Quotes

class QuotesTests: XCTestCase {

    func testLiveApi_connect_Returns_QuotesCategory() {
        var cancellables = Set<AnyCancellable>()
        let exceptation = XCTestExpectation(description: "Publisher decoded the items")
         let quotesService = QuotesCategoryService()
          quotesService
            .fetch()
            .sink(
                receiveCompletion: { error in
                    print(error)
                },
                receiveValue: { items in
                    print(items)
                    XCTAssertEqual(items.success?.total, 8)
                    exceptation.fulfill()
            }).store(in: &cancellables)
         
        wait(for: [exceptation], timeout: 50)
    }
    
}
