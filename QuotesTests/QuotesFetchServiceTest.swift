//
//  QuotesFetchServiceTest.swift
//  QuotesTests
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import XCTest
import Combine
@testable import Quotes


class QuotesFetchServiceTest: XCTestCase {
    
    func test_WhenRequestSucceds_Decodes_items() throws {
        let json = """
          {
              "success": {
                  "total": 1
              },
              "contents": {
                  "quotes": [
                      {
                          "quote": "I keep asking myself these three questions.. What do you have? What do you want? What will you give up?",
                          "length": "105",
                          "author": "Jack Ma",
                          "tags": [
                              "barriers",
                              "inspire",
                              "mind"
                          ],
                          "category": "inspire",
                          "language": "en",
                          "date": "2022-02-27",
                          "permalink": "https://theysaidso.com/quote/jack-ma-i-keep-asking-myself-these-three-questions-what-do-you-have-what-do-you",
                          "id": "7o1kOJyXP0E_t7Z2IXyXuweF",
                          "background": "https://theysaidso.com/img/qod/qod-inspire.jpg",
                          "title": "Inspiring Quote of the day"
                      }
                  ]
              },
              "baseurl": "https://theysaidso.com",
              "copyright": {
                  "year": 2024,
                  "url": "https://theysaidso.com"
              }
          }
       """
        let data = try XCTUnwrap(json.data(using: .utf8))
        var cancellables = Set<AnyCancellable>()
        let networkSeverice = NetworkServiceProtocolSub(result: .success(data))
        let quotesFetcher = QuotesFetchService(networkService: networkSeverice)
        let exceptation = XCTestExpectation(description: "Decode the items")
        
        quotesFetcher
            .fetchQuotes(category: "")
            .sink(receiveCompletion: {_ in }) { quotes in
                XCTAssertEqual(quotes.contents?.quotes.count, 1)
                exceptation.fulfill()
            }.store(in: &cancellables)
    }
    
    func test_QuoteService_Returns_Error() {
        let error = URLError(.badServerResponse)
        var cancellables = Set<AnyCancellable>()
        let expectation = XCTestExpectation(description: "Testing network erro")
        
        let networkSeverice = NetworkServiceProtocolSub(result: .failure(error))
        let quotesFetcher = QuotesFetchService(networkService: networkSeverice)
        
        quotesFetcher
            .fetchQuotes(category: "")
            .sink(receiveCompletion: { completion in
                guard case .failure( let error) = completion else {
                    return
                }
                XCTAssertEqual(error as? URLError, error as! URLError)
                expectation.fulfill()
                
            }, receiveValue: { _ in
               XCTFail("Recieved items")
            }).store(in: &cancellables)
        
    }
    
}
