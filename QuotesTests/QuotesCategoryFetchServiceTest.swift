//
//  QuotesCategoryFetchServiceTest.swift
//  QuotesTests
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import XCTest
import Combine
@testable import Quotes

class QuotesCategoryFetchServiceTest: XCTestCase {

    func test_WhenRequestSucceds_Decodes_items() throws {
       let json = """
            {
           "success": {
               "total": 8
           },
           "contents": {
               "categories": {
                   "inspire": "Inspiring Quote of the day",
                   "management": "Management Quote of the day",
                   "sports": "Sports Quote of the day",
                   "life": "Quote of the day about life",
                   "funny": "Funny Quote of the day",
                   "love": "Quote of the day about Love",
                   "art": "Art quote of the day ",
                   "students": "Quote of the day for students"
               }
           },
           "baseurl": "https://theysaidso.com",
           "copyright": {
               "year": 2024,
               "url": "https://theysaidso.com"
           }
         }
       """
        let data = try XCTUnwrap(json.data(using: .utf8))
        var cancellables = Set<AnyCancellable>()
        let networkSeverice = NetworkServiceProtocolSub(result: .success(data))
        let quotesFetcher = QuotesCategoryService(networkService: networkSeverice)
        let exceptation = XCTestExpectation(description: "Decode the items")
        
        quotesFetcher
            .fetch()
            .sink(receiveCompletion: {_ in }) { quotes in
                XCTAssertEqual(quotes.contents?.categories?.count,8)
                exceptation.fulfill()
            }.store(in: &cancellables)
    }
}
