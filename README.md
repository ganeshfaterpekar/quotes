# Quotes

## Overview

Develop a native iOS application that makes a call to a simple (RESTful) web service, and displays the data that is returned in the response on the screen of the mobile app.

API to be consume https://theysaidso.com/api/

## System Requirements

* Xcode 13.x
* iOS 13
* Swift Package Manager

## Setup

```
git clone https://gitlab.com/ganeshfaterpekar/quotes.git

```

### Architecture
## Code 
Code follows MVVM design pattern combined with SOLID Principles. The code is divided into different group / layers (Self explanatory)         




## Requirement Traceability


| Requirement                        |   Implemented        | Comments                                           |
| -------------                      |    :-------------:   | :-----                                             |
| Swift langauge                     | :white_check_mark:    | Done using Swift UI and Combine    |
| Xcode 13                           | :white_check_mark:    |                     |
| Desgin Patterns                    | :white_check_mark:    | Solid Principles,MVVM, Dependency Injection followed |
| Dependency Management              | :white_check_mark:    | SPM   |
| Testing                            | :white_check_mark:    | Unit testing Added |

