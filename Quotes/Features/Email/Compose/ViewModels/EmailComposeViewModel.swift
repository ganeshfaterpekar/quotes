//
//  EmailComposeViewModel.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation
import Combine

class EmailComposeViewModel : ObservableObject {
    var email : Email = Email()
    
}

struct Email {
    let title: String = ""
    let body: String = ""
}
