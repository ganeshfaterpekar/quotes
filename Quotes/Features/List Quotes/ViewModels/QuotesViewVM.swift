//
//  QuotesViewViewModel.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 1/3/22.
//

import Foundation
import Combine

class QuotesViewVM: ObservableObject {
    @Published var quote : Quote?
    var category : String
    
    var cancellables = Set<AnyCancellable>()
    var quotesService = QuotesFetchService()

    
    init(category: String = "") {
        self.category = category
    }
    
    func getQuotes() {
      let cancellable =  quotesService
            .fetchQuotes(category: category)
            .sink(
                receiveCompletion: { completion in
                
            },
                receiveValue: { quotes in
                    if let quote = quotes.contents?.quotes {
                        DispatchQueue.main.async {
                            self.quote = quote.first
                        }
                    }
            })
        
        cancellables.insert(cancellable)
        
    }
}
