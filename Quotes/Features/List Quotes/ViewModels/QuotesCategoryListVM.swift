//
//  QuotesCategoryListViewModel.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 28/2/22.
//

import Foundation
import Combine
import SwiftUI

class QuotesCategoryListVM : ObservableObject {
    @Published var categories = Array<Categories>()
    
    var cancellables = Set<AnyCancellable>()
    var quotescategoryService = QuotesCategoryService()

    func getQuotesCategories() {
        let cancellable = quotescategoryService
            .fetch()
            .sink(receiveCompletion: {  result in 
                switch result {
                case .failure(let error):
                    print("Handle error: \(error)")
                case .finished:
                    print("finished")
                    break
                }
        
            },
            receiveValue: { quotes in
               if let list = quotes.contents?.categories {
                   var categoriesArray = Array<Categories>()
                   for(key, value) in list {
                       let category = Categories(name: key, desc: value)
                       categoriesArray.append(category)
                   }
                   print(categoriesArray)
                   DispatchQueue.main.async {
                       self.categories = categoriesArray
                   }
                   
               }
        })
        cancellables.insert(cancellable)
    }
}



