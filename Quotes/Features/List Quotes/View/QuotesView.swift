//
//  QuotesView.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 1/3/22.
//

import SwiftUI

struct QuotesView: View {
    @ObservedObject var viewModel = QuotesViewVM()
    @State private var inputs: [String] = Array(repeating: "", count: 3)
    

    var body: some View {
        VStack {
            Text(viewModel.quote?.title ?? "")
                .fontWeight(.bold)
                .font(.largeTitle)
                .padding()
            Spacer()
            Text(viewModel.quote?.quote ?? "")
                .font(.title)
                .padding()
            Spacer()
            Text("Share your favourite quotes")
                .font(.title2)
            ForEach(inputs.indices) { index in
                TextField("Enter Email address", text: $inputs[index])
                    .textFieldStyle(.roundedBorder)
                    .padding(.vertical,5)
                    .padding(.horizontal,5)
            }
            Spacer()
            Button {
                
            } label: {
                Text("Send")
            }.padding()
                .background(Color.yellow)
                .foregroundColor(Color.black)
                .cornerRadius(5.0)

        }.padding()
        .onAppear {
            viewModel.getQuotes()
        }
    }
}

struct QuotesView_Previews: PreviewProvider {
    static var previews: some View {
        QuotesView()
    }
}
