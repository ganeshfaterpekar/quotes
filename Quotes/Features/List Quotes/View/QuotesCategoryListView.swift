//
//  QuotesListView.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 28/2/22.
//

import SwiftUI
import Combine

struct QuotesCategoryListView: View {
    @ObservedObject var viewModel = QuotesCategoryListVM()
   
    var body: some View {
        NavigationView {
            List {
                ForEach(viewModel.categories, id:\.self) { category in
                NavigationLink {
                    QuotesView(viewModel: QuotesViewVM(category: category.name))
                   } label: {
                      Text(category.name)
                  }
                }.padding()
          }//.navigationTitle("Categories")
        }.onAppear {
            self.viewModel.getQuotesCategories()
        }
       
        
    }
}
struct QuotesListView_Previews: PreviewProvider {
    static var previews: some View {
        QuotesCategoryListView()
          
    }
}
