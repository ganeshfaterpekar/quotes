//
//  NetworkServiceProtocol.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation
import Combine

protocol NetworkServiceProtocol {
    func loadRequest<T:Decodable>(urlRequest : URLRequest) -> AnyPublisher<T,Error>
}

extension URLSession: NetworkServiceProtocol {
    func loadRequest<T>(urlRequest: URLRequest) -> AnyPublisher<T, Error> where T : Decodable  {
        return dataTaskPublisher(for: urlRequest)
            .tryMap { result in
                let decoder = JSONDecoder()
                return try decoder.decode(T.self, from: result.data)
            }.eraseToAnyPublisher()
    }

}


