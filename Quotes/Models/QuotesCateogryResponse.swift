//
//  QuotesCateogryResponse.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation
import Combine
struct QuotesCateogryResponse: Decodable {
    let contents: CategoryContents?
    let success : Success?
}

struct CategoryContents : Decodable {
    let categories : [String:String]?
}


