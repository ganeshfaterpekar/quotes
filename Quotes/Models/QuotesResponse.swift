//
//  Quotes.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation

struct QuotesResponse : Decodable {
    let baseurl: String?
    let contents : Contents?
    let success: Success?
}

// MARK: - Contents
struct Contents: Decodable {
    let quotes: [Quote]
}

// MARK: - Quote
struct Quote: Decodable {
    let quote : String?
    let category: String?
    let date: String?
    let title : String?
}
extension Quote {
    var titleValue : String {
        if let value = title {
            return value
        }
     return ""
    }
    
    var quoteValue : String {
        if let value = quote {
            return value
        }
     return ""
    }
    
}

// MARK: - Success
struct Success: Decodable {
    let total: Int?
}
