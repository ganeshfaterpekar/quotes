//
//  Categories.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 28/2/22.
//

import Foundation

struct Categories : Decodable, Equatable,Hashable ,Identifiable {
    var id = UUID()
    let name: String
    let desc: String
}
