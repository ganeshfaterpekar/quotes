//
//  QuotesApp.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import SwiftUI

@main
struct QuotesApp: App {
    var body: some Scene {
        WindowGroup {
            QuotesCategoryListView()
        }
    }
}
