//
//  QuotesFetchServiceProtocol.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation
import Combine

protocol QuotesFetchServiceProtocol {
    func fetchQuotes(category: String) -> AnyPublisher<QuotesResponse,Error>
    
}
