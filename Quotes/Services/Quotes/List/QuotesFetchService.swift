//
//  QuotesFetchService.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation
import Combine

class QuotesFetchService : QuotesFetchServiceProtocol {
    
    let networkService : NetworkServiceProtocol
    
    init(networkService:  NetworkServiceProtocol = URLSession.shared) {
        self.networkService = networkService
    }
    
    func fetchQuotes(category: String) -> AnyPublisher<QuotesResponse, Error> {
        let url = URL(string:"https://quotes.rest/qod.json?category=\(category)")!
        return networkService
            .loadRequest(urlRequest: URLRequest(url: url))
    }

}
