//
//  QuotesCategoryServiceProtocol.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation
import Combine

protocol QuotesCategoryServiceProtocol {
    func fetch() -> AnyPublisher<QuotesCateogryResponse,Error>
}
