//
//  QuotesCategoryService.swift
//  Quotes
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation
import Combine

class QuotesCategoryService : QuotesCategoryServiceProtocol {
    
    let networkService : NetworkServiceProtocol
    
    init(networkService:  NetworkServiceProtocol = URLSession.shared) {
        self.networkService = networkService
    }
    
    func fetch() -> AnyPublisher<QuotesCateogryResponse, Error> {
        let url = URL(string:"https://quotes.rest/qod/categories.json")!
        return networkService
            .loadRequest(urlRequest: URLRequest(url: url))
    }

}
